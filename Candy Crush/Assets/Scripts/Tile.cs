using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    //variable x y
    public int xIndex;
    public int yIndex;

    //variable que determina el tablero
    Board m_board;

    //variables para efectos de sonido del juego
    public AudioSource source;
    public AudioClip audioFX;
    public GameObject[] pref;

    //Esta es la funcion que va a inicializar todos los indices y el tablero
    public void Init(int cambioX, int cambioY, Board board)
    {
        xIndex = cambioX;
        yIndex = cambioY;

        m_board = board;
    }

    //Esta es la funcion que va a detectar cada vez que se haga un click en el tile.
    public void OnMouseDown()
    {
        m_board.ClickedTile(this);
    }

    //Esta es la funcion que va a detectar cada vez que el mouse entre en otra casilla u otro tile
    public void OnMouseEnter()
    {
        m_board.DragToTile(this);
    }

    //Esta es la funcion que va a rreconocer cada que el mouse se suelte, y a su vez ejecutar el sonido y ejecuta lo antes dicho.
    public void OnMouseUp()
    {
        m_board.ReleaseTile();

        //Cuando se selecciona una ficha esta sonara.

        AudioSource.PlayClipAtPoint(audioFX, gameObject.transform.position);
    }
}
