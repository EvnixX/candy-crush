using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class Puntaje : MonoBehaviour
{
    //variable de enteros para los puntos del juego
    private int puntos;

    //variable para determinar los movimientos necesarios
    public int movimientosNecesarios;

    //variable para conseguir un puntaje maximo 
    [SerializeField] private int goalScore;

    //Variable text mesh pro para mostrar el puntaje en la pantalla de juego
    private TextMeshProUGUI TextMesh;

    //se llama al script de contador y asi poder utilizarlo aqui 
    public Contador tiempo;

    //variable de opciones para saber de que manera ganara el jugador en cada nivel
    public WinCondition _winCondition;

    //En el start se llama al text mesh pro y se convierten los puntos en string para que se puedan mostrar en la pantalla de juego
    private void Start()
    {
        TextMesh = GetComponent<TextMeshProUGUI>();
        TextMesh.text = puntos.ToString("0");
    }

    //Aqui se permite la sumatoria de puntos para asi poder calcular la cantidad de puntos que tiene el jugador
    //Esa cantidad se convierte en un string para que el jugador pueda verlo en pantalla.
    public void SumatoriaPuntos(int puntosEntrada)
    {
        puntos += puntosEntrada;
        TextMesh.text = puntos.ToString();
    }

    //Se utiliza un switch para que las opciones dependan de un enumerador y obtener un metodo de victoria
    public void SetCondition()
    {
        switch (_winCondition)
        {
            case WinCondition.Time:

                if (tiempo.restante >= 1 && puntos >= goalScore)
                {
                    SceneManager.LoadScene("Ganaste");
                }
                else
                {
                    SceneManager.LoadScene("Game over");
                }

                break;
            case WinCondition.Score:

                if (puntos >= goalScore)
                {
                    SceneManager.LoadScene("Ganaste");
                }
                else
                {
                    SceneManager.LoadScene("Game over");
                }

                break;
            case WinCondition.mix:

                if (tiempo.restante >= 0 && puntos >= goalScore)
                {
                    SceneManager.LoadScene("Ganaste");
                }
                else
                {
                    SceneManager.LoadScene("Game over");
                }

                break;
        }
    }

    //Este es el enumerador que nos ayudara a elegir la opcion de victoria en cada nivel
    public enum WinCondition
    {
        Time,
        Score,
        mix,
    }
}
