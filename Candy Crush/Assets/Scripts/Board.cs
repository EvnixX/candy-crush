using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.SceneManagement;

public class Board : MonoBehaviour
{
	//Variables de ancho y alto
	public int width;
	public int height;

	//Variable del controlador del borde dado a la camara.
	public int margen;


	//variable del tile y el array para los prefab.
	public GameObject tilePrefab;
	public GameObject[] gamePiecesPrefabs;

	//variable del tiempo de movimiento de las piezas
	public float swapTime = .3f;

	//Con esta variable se llama al scrip del puntaje para asi poder usarlo en el board.
	public Puntaje m_puntaje;

	//Variable de los arrays de los tile y las fichas a utilizar.
	Tile[,] m_allTiles;
	GamePiece[,] m_allGamePieces;

	//Variables declaradas para las fichas seleccionadas
	[SerializeField] Tile m_clickedTile;
	[SerializeField] Tile m_targetTile;

	//variable booleana para controlar y determinar los movimientos del jugador
	bool m_playerInputEnabled = true;

	//Variable para el control de la camara
	Transform tileParent;
	Transform gamePieceParent;

	//controlador de combos en el juego
	int myCount = 0;

	//Variables para el sonido a utilizar en el juego
	public AudioSource source;
	public AudioClip audioFX;
	public AudioClip destroyAudio;


	private void Start()
	{
		SetParents();

		m_allTiles = new Tile[width, height];
		m_allGamePieces = new GamePiece[width, height];

		SetupTiles();
		SetupCamera();
		FillBoard(10, .5f);
	}

	//modificamos los transforms de los gameobjects
	private void SetParents()
	{
		if (tileParent == null)
		{
			tileParent = new GameObject().transform;
			tileParent.name = "Tiles";
			tileParent.parent = this.transform;
		}

		if (gamePieceParent == null)
		{
			gamePieceParent = new GameObject().transform;
			gamePieceParent.name = "GamePieces";
			gamePieceParent = this.transform;
		}
	}

	//controlaodor de camara y modificador de posicion de la camara en el aspecto del juego
	private void SetupCamera()
	{
		Camera.main.transform.position = new Vector3((float)(width - 1) / 2f, (float)(height - 1) / 2f, -10f);

		float aspectRatio = (float)Screen.width / (float)Screen.height;
		float verticalSize = (float)height / 2f + (float)margen;
		float horizontalSize = ((float)width / 2f + (float)margen) / aspectRatio;
		Camera.main.orthographicSize = verticalSize > horizontalSize ? verticalSize : horizontalSize;
	}

	//esta funcion setea los tiles, luego los pone en los tiles  y los ubica en una cuadricula
	private void SetupTiles()
	{
		for (int i = 0; i < width; i++)
		{
			for (int j = 0; j < height; j++)
			{
				GameObject tile = Instantiate(tilePrefab, new Vector2(i, j), Quaternion.identity);
				tile.name = $"Tile({i},{j})";

				if (tileParent != null)
				{
					tile.transform.parent = tileParent;
				}
				m_allTiles[i, j] = tile.GetComponent<Tile>();
				m_allTiles[i, j].Init(i, j, this);
			}
		}
	}

	//permite rellenar la matriz de piezas y en sus respectivas ubiciones segun el tile
	//tiene en cuenta el numero de veces en las que se puede hacer match apenas se inicialice y cuando este llegue a su limite saltara advertencia
	//rellena la matriz en el momento que note espacios.
	private void FillBoard(int falseOffset = 0, float moveTime = .1f)
	{
		List<GamePiece> addedPieces = new List<GamePiece>();

		for (int i = 0; i < width; i++)
		{
			for (int j = 0; j < height; j++)
			{
				if (m_allGamePieces[i, j] == null)
				{
					if (falseOffset == 0)
					{
						GamePiece piece = FillRandomAt(i, j);
						addedPieces.Add(piece);
					}
					else
					{
						GamePiece piece = FillRandomAt(i, j, falseOffset, moveTime);
						addedPieces.Add(piece);
					}
				}
			}
		}
		int maxIterations = 20;
		int iterations = 0;

		bool isFilled = false;

		while (!isFilled)
		{
			List<GamePiece> matches = FindAllMatches();

			if (matches.Count == 0)
			{
				isFilled = true;
				break;
			}
			else
			{
				matches = matches.Intersect(addedPieces).ToList();

				if (falseOffset == 0)
				{
					ReplaceWithRandom(matches);
				}
				else
				{
					ReplaceWithRandom(matches, falseOffset, moveTime);
				}
			}

			if (iterations > maxIterations)
			{
				isFilled = true;
				Debug.LogWarning($"Board.FillBoard alcanzo el maximo de interacciones, abortar");
			}

			iterations++;
		}
	}

	//te permite la seleccion de una casilla 
	public void ClickedTile(Tile tile)
	{
		if (m_clickedTile == null)
		{
			m_clickedTile = tile;
		}
	}

	//te permite arrastrar el mouse con una ficha y cambiarla con otra
	public void DragToTile(Tile tile)
	{
		if (m_clickedTile != null && IsNextTo(tile, m_clickedTile))
		{
			m_targetTile = tile;
		}
	}

	//con las dos fichas selecionadas se les cambia de sitio y se resetea
	public void ReleaseTile()
	{
		if (m_clickedTile != null && m_targetTile != null)
		{
			SwitchTiles(m_clickedTile, m_targetTile);
		}
		m_clickedTile = null;
		m_targetTile = null;
	}

	//Activa una corrutina de switches
	private void SwitchTiles(Tile m_clickedTile, Tile m_targetTile)
	{
		StartCoroutine(SwitchTilesRoutine(m_clickedTile, m_targetTile));
	}

	//Es una corrutina que nos permite cambiar las piezas de sitio con sus indices de coordenada en la matriz 
	//limita el tiempo de cambio de las mismas y si realiza un match lo que hara es rellenar la matriz
	IEnumerator SwitchTilesRoutine(Tile clickedTile, Tile targetTile)
	{
		if (m_playerInputEnabled)
		{
			GamePiece clickedPiece = m_allGamePieces[clickedTile.xIndex, clickedTile.yIndex];
			GamePiece targetPiece = m_allGamePieces[targetTile.xIndex, targetTile.yIndex];

			if (clickedPiece != null && targetPiece != null)
			{
				clickedPiece.Move(targetTile.xIndex, targetTile.yIndex, swapTime);
				targetPiece.Move(clickedPiece.xIndex, clickedPiece.yIndex, swapTime);

				yield return new WaitForSeconds(swapTime);

				List<GamePiece> clickedPieceMatches = FindMatchesAt(clickedTile.xIndex, clickedTile.yIndex);
				List<GamePiece> targetPieceMatches = FindMatchesAt(targetTile.xIndex, targetTile.yIndex);

				if (clickedPieceMatches.Count == 0 && targetPieceMatches.Count == 0)
				{
					clickedPiece.Move(clickedTile.xIndex, clickedTile.yIndex, swapTime);
					targetPiece.Move(targetTile.xIndex, targetTile.yIndex, swapTime);

					yield return new WaitForSeconds(swapTime);

				}
				else
				{
					//Al destruirse un match suena.
					CleatAndRefillBoard(clickedPieceMatches.Union(targetPieceMatches).ToList());
					AudioSource.PlayClipAtPoint(destroyAudio, gameObject.transform.position);
				}
				
			}
		}
		
	}

	//Activa la corrutina de CleatAbdRefillBoard y mantiene el contador de combos en cero
	private void CleatAndRefillBoard(List<GamePiece> gamePieces)
	{
		myCount = 0;
		StartCoroutine(ClearAndRefillRoutine(gamePieces));
	}

	//en esta lista se encuentran las coincidencias en toda la matriz
	//se le determina un tama�o minimo pra los matches
	List<GamePiece> FindMatches(int startX, int startY, Vector2 searchDirection, int minLenght = 3)
	{
		List<GamePiece> matches = new List<GamePiece>();
		GamePiece startPiece = null;

		if (IsWithBounds(startX, startY))
		{
			startPiece = m_allGamePieces[startX, startY];
		}

		if (startPiece != null)
		{
			matches.Add(startPiece);
		}
		else
		{
			return null;
		}

		int nextX;
		int nextY;

		int maxValue = width > height ? width : height;

		for (int i = 1; i < maxValue; i++)
		{
			nextX = startX + (int)Mathf.Clamp(searchDirection.x, -1, 1) * i;
			nextY = startY + (int)Mathf.Clamp(searchDirection.y, -1, 1) * i;

			if (!IsWithBounds(nextX, nextY))
			{
				break;
			}

			GamePiece nextPiece = m_allGamePieces[nextX, nextY];

			if (nextPiece == null)
			{
				break;
			}
			else
			{
				if (nextPiece.tipoFicha == startPiece.tipoFicha && !matches.Contains(nextPiece))
				{
					matches.Add(nextPiece);
				}
				else
				{
					break;
				}
			}
		}

		if (matches.Count >= minLenght)
		{
			return matches;
		}
		else
		{
			return null;
		}
	}

	//esta lista busca y encuentra matches de manera vertical, combinando listas de arriba y abajo 
	List<GamePiece> FindVerticalMatches(int startX, int startY, int minLenght = 3)
	{
		List<GamePiece> upwardMatches = FindMatches(startX, startY, Vector2.up, 2);
		List<GamePiece> downwardMatches = FindMatches(startX, startY, Vector2.down, 2);

		if (upwardMatches == null)
		{
			upwardMatches = new List<GamePiece>();
		}
		if (downwardMatches == null)
		{
			downwardMatches = new List<GamePiece>();
		}

		var combinedMatches = upwardMatches.Union(downwardMatches).ToList();
		return combinedMatches.Count >= minLenght ? combinedMatches : null;
	}

	//esta lista buca y encuentra matches de manera horizontal, combinando listas de izquierda y derecha 
	List<GamePiece> FindHorizontalMatches(int startX, int startY, int minLenght = 3)
	{
		List<GamePiece> rightMatches = FindMatches(startX, startY, Vector2.right, 2);
		List<GamePiece> leftMatches = FindMatches(startX, startY, Vector2.left, 2);

		if (rightMatches == null)
		{
			rightMatches = new List<GamePiece>();
		}
		if (leftMatches == null)
		{
			leftMatches = new List<GamePiece>();
		}

		var combinedMatches = rightMatches.Union(leftMatches).ToList();
		return combinedMatches.Count >= minLenght ? combinedMatches : null;
	}

	//esta lista combina la lista de matches en vertical y horizontal para que se genere una busueuda en las dos direcciones
	//tambien te da puntos al crearce un L o T 
	private List<GamePiece> FindMatchesAt(int x, int y, int minLenght = 3)
	
	{
		List<GamePiece> horizontalMatches = FindHorizontalMatches(x, y, minLenght);
		List<GamePiece> verticalMatches = FindVerticalMatches(x, y, minLenght);

		int vecino = 0;

		if (horizontalMatches == null)
		{
			horizontalMatches = new List<GamePiece>();
		}
		if (verticalMatches == null)
		{
			verticalMatches = new List<GamePiece>();
		}
		var combinedMatches = horizontalMatches.Union(verticalMatches).ToList();

		if(horizontalMatches.Count > 0 && verticalMatches.Count > 0)
        {
            for (int i = 1; i < horizontalMatches.Count; i++)
            {
				if(IsNextTo(horizontalMatches[0], horizontalMatches[i]))
                {
					Debug.Log("Vecino en horizontal");
					vecino++;
				}
            }

            for (int i = 1; i < verticalMatches.Count; i++)
            {
				if(IsNextTo(horizontalMatches[0],verticalMatches[i]))
                {
					Debug.Log("Vecino en vertical");
					vecino++;
				}
				

            }
			if(vecino >= 3)
            {
				Debug.Log("T");
            }
			if(vecino == 2)
            {
				Debug.Log("L");
            }
			

		}

		return combinedMatches;
	}

	//revisa si el vecino que tiene es del mismo tipo que la ficha
	private bool IsNextTo(Tile start, Tile end)
	{
		if (Mathf.Abs(start.xIndex - end.xIndex) == 1 && start.yIndex == end.yIndex)
		{
			return true;
		}

		if (Mathf.Abs(start.yIndex - end.yIndex) == 1 && start.xIndex == end.xIndex)
		{
			return true;
		}

		return false;
	}

	//este booleano lo que hace es revisar si los vecinos de la ficha son iguales a el
	private bool IsNextTo(GamePiece start, GamePiece end)
	{
		if (Mathf.Abs(start.xIndex - end.xIndex) == 1 && start.yIndex == end.yIndex)
		{
			return true;
		}

		if (Mathf.Abs(start.yIndex - end.yIndex) == 1 && start.xIndex == end.xIndex)
		{
			return true;
		}

		return false;
	}

	//se encarga de revisar si hay coincidencias en la lista que se cre� y luego almacena
	List<GamePiece> FindMatchesAt(List<GamePiece> gamePieces, int minLenght = 3)
	{
		List<GamePiece> matches = new List<GamePiece>();

		foreach (GamePiece piece in gamePieces)
		{
			matches = matches.Union(FindMatchesAt(piece.xIndex, piece.yIndex, minLenght)).ToList();
		}

		return matches;
	}

	//busca en la lista de FindMatchesAt que se pueda tener tanto alto como ancho
	private List<GamePiece> FindAllMatches()
	{
		List<GamePiece> combinedMatches = new List<GamePiece>();

		for (int i = 0; i < width; i++)
		{
			for (int j = 0; j < height; j++)
			{
				var matches = FindMatchesAt(i, j);
				combinedMatches = combinedMatches.Union(matches).ToList();
			}
		}

		return combinedMatches;
	}

	//apaga el resaltado del tile en el momneto en el que se efecute una coincidencia
	void HighlightTileOff(int x, int y)
	{
		SpriteRenderer spriteRender = m_allTiles[x, y].GetComponent<SpriteRenderer>();
		spriteRender.color = new Color(spriteRender.color.r, spriteRender.color.g, spriteRender.color.b, 0);
	}

	//resalta los tile cuando se efectua una coincidencia y muestra en donde esta sucedio
	void HighlightTileOn(int x, int y, Color col)
	{
		SpriteRenderer spriteRenderer = m_allTiles[x, y].GetComponent<SpriteRenderer>();
		spriteRenderer.color = col;
	}

	//resalta los tile al momento en el que se producen coincidencias de manera natural
	void HighlightMatchesAt(int x, int y)
	{
		HighlightTileOff(x, y);
		var combinedMatches = FindMatchesAt(x, y);

		if (combinedMatches.Count > 0)
		{
			foreach (GamePiece piece in combinedMatches)
			{
				HighlightTileOn(piece.xIndex, piece.yIndex, piece.GetComponent<SpriteRenderer>().color);
			}
		}
	}

	//revisa el largo y el ancho de la matriz de tile para asi resaltar al momento que se realice una coincidencia
	void HighlightMatches()
	{
		for (int i = 0; i < width; i++)
		{
			for (int j = 0; j < height; j++)
			{
				HighlightMatchesAt(i, j);
			}
		}
	}

	//recibe una lista en la que se realiza el realtado del tile
	void HighlightPieces(List<GamePiece> gamepieces)
	{
		foreach (GamePiece piece in gamepieces)
		{
			if (piece != null)
			{
				HighlightTileOn(piece.xIndex, piece.yIndex, piece.GetComponent<SpriteRenderer>().color);
			}
		}
	}

	//destruye las fichas al momento de hacer la coincidencia 
	//invoca el resaltado de los tiles
	void ClearPieceAt(int x, int y)
	{
		GamePiece pieceToClear = m_allGamePieces[x, y];

		if (pieceToClear != null)
		{
			m_allGamePieces[x, y] = null;

			Destroy(pieceToClear.gameObject);

			//Suena cuando se destruye al hacer match y al caer despues de otro match

			AudioSource.PlayClipAtPoint(destroyAudio, gameObject.transform.position);
		}

		HighlightTileOff(x, y);
	}

	//esta es la sobrecarga del alterior ClearPieceAt, se hace para revisar la lista de piezas que le brindamos y asi poder ejecutar la destruccion de las fichas
	void ClearPieceAt(List<GamePiece> gamePieces)
	{
		foreach (GamePiece piece in gamePieces)
		{
			if (piece != null)
			{
				ClearPieceAt(piece.xIndex, piece.yIndex);
			}
		}
	}

	//revisa el alto y ancho de la matriz y dice donde debe limpiarce 
	void ClearBoard()
	{
		for (int i = 0; i < width; i++)
		{
			for (int j = 0; j < height; j++)
			{
				ClearPieceAt(i, j);
			}
		}
	}

	//obtiene una pieza ramdon para poner en la matriz 
	GameObject GetRandomPiece()
	{
		int randomInx = Random.Range(0, gamePiecesPrefabs.Length);

		if (gamePiecesPrefabs[randomInx] == null)
		{
			Debug.LogWarning($"La clase Board en el array de prefabs en la posicion {randomInx} no contiene una pieza valida");
		}

		return gamePiecesPrefabs[randomInx];
	}

	//esta funcion viene y pone en la mariz una pieza que se haya selecionado para asi rellenar la matriz en el momento que las piezas desaparezcan 
	public void placeGamePiece(GamePiece gamePiece, int x, int y)
	{
		if (gamePiece == null)
		{
			Debug.LogWarning($"gamePiece invalida");
			return;
		}

		gamePiece.transform.position = new Vector2(x, y);
		gamePiece.transform.rotation = Quaternion.identity;

		if (IsWithBounds(x, y))
		{
			m_allGamePieces[x, y] = gamePiece;
		}

		gamePiece.SetCoord(x, y);
	}

	//esta funcion retorna en un entero para que se recorra el array 
	private bool IsWithBounds(int x, int y)
	{
		return (x >= 0 && x < width && y >= 0 && y < height);
	}

	//rellena la matriz con una ficha ramdon 
	GamePiece FillRandomAt(int x, int y, int falseOffset = 0, float moveTime = .1f)
	{
		GamePiece randomPiece = Instantiate(GetRandomPiece(), Vector2.zero, Quaternion.identity).GetComponent<GamePiece>();

		if (randomPiece != null)
		{
			randomPiece.Init(this);
			placeGamePiece(randomPiece, x, y);

			if (falseOffset != 0)
			{
				randomPiece.transform.position = new Vector2(x, y + falseOffset);
				randomPiece.Move(x, y, moveTime);
			}

			randomPiece.transform.parent = gamePieceParent;
		}

		return randomPiece;
	}

	//se reemplazan fichas en la matriz
	void ReplaceWithRandom(List<GamePiece> gamePieces, int falseOffset = 0, float moveTime = .1f)
	{
		foreach (GamePiece piece in gamePieces)
		{
			ClearPieceAt(piece.xIndex, piece.yIndex);

			if (falseOffset == 0)
			{
				FillRandomAt(piece.xIndex, piece.yIndex);
			}
			else
			{
				FillRandomAt(piece.xIndex, piece.yIndex, falseOffset, moveTime);
			}
		}
	}

	//permite que las columnas se caigan para asi poder rellenar los espacion en la matriz
	List<GamePiece> CollapseColumn(int column, float collapseTime = .1f)
	{
		List<GamePiece> movingPieces = new List<GamePiece>();

		for (int i = 0; i < height - 1; i++)
		{
			if (m_allGamePieces[column, i] == null)
			{
				for (int j = i + 1; j < height; j++)
				{
					if (m_allGamePieces[column, j] != null)
					{
						m_allGamePieces[column, j].Move(column, i, collapseTime * (j - i));
						m_allGamePieces[column, i] = m_allGamePieces[column, j];
						m_allGamePieces[column, i].SetCoord(column, i);

						if (!movingPieces.Contains(m_allGamePieces[column, i]))
						{
							movingPieces.Add(m_allGamePieces[column, i]);
						}

						m_allGamePieces[column, j] = null;
						break;
					}
				}
			}
		}

		return movingPieces;
	}

	//permite que las fichas bajen para hacer el efecto de caida de las fichas
	List<GamePiece> CollapseColumn(List<GamePiece> gamePieces)
	{
		List<GamePiece> movingPieces = new List<GamePiece>();
		List<int> columnsToColapse = GetColumns(gamePieces);

		foreach (int column in columnsToColapse)
		{
			movingPieces = movingPieces.Union(CollapseColumn(column)).ToList();
		}

		return movingPieces;
	}

	//aqui se obtienen las columnas de la matriz
	private List<int> GetColumns(List<GamePiece> gamePieces)
	{
		List<int> columns = new List<int>();

		foreach (GamePiece piece in gamePieces)
		{
			if (!columns.Contains(piece.xIndex))
			{
				columns.Add(piece.xIndex);
			}
		}

		return columns;
	}

	//ayuda a limpiar y rellenar la matriz
	//nos proporciona los puntos al momento de hacer las coincidencias
	IEnumerator ClearAndRefillRoutine(List<GamePiece> gamePieces)
	{
		m_playerInputEnabled = true;
		List<GamePiece> matches = gamePieces;

		do
		{
			//esta es la parte donde se activa la animacion al hacer match manual//
			foreach (GamePiece piece in matches)
			{
				if (matches.Count == 3)
				{
					int cantidadPuntos = 10;

					m_puntaje.SumatoriaPuntos(cantidadPuntos);

				}
				if (matches.Count == 4)
				{
					int cantidadPuntos = 20;

					m_puntaje.SumatoriaPuntos(cantidadPuntos);

				}
				if (matches.Count == 5)
				{
					int cantidadPuntos = 30;

					m_puntaje.SumatoriaPuntos(cantidadPuntos);

				}
				if (matches.Count >= 6)
				{
					int cantidadPuntos = 40;

					m_puntaje.SumatoriaPuntos(cantidadPuntos);

				}



			}
				m_puntaje.movimientosNecesarios -= 1;
		
			yield return StartCoroutine(ClearAndCollapseRoutine(matches));
			yield return null;
			yield return StartCoroutine(RefillRoutine());
			matches = FindAllMatches();
			yield return new WaitForSeconds(.5f);
		}
		while (matches.Count != 0);
		m_playerInputEnabled = true;
	}

	//ayuda a limpiar la matriz y a colapsar las columnas al teminar las coincidencias y las combinaciones realizadas
	//brinda los puntos al momento de generase una coincidencia natural
	//multiplica el puntaje si los combos aumentan
	IEnumerator ClearAndCollapseRoutine(List<GamePiece> gamePieces)
	{
		myCount++;

		List<GamePiece> movingPieces = new List<GamePiece>();
		List<GamePiece> matches = new List<GamePiece>();

		HighlightPieces(gamePieces);

		yield return new WaitForSeconds(.5f);
		bool isFinished = false;


		while (!isFinished)
		{
			ClearPieceAt(gamePieces);
			yield return new WaitForSeconds(.25f);

			movingPieces = CollapseColumn(gamePieces);
			while (!IsCollapsed(gamePieces))
			{
				yield return null;
			}

			//Cuando se rellena la matriz tiene sonido

			AudioSource.PlayClipAtPoint(audioFX, gameObject.transform.position);

			yield return new WaitForSeconds(.5f);

			matches = FindMatchesAt(movingPieces);

			if (matches.Count == 0)
			{
				isFinished = true;
				break;
			}
			else
			{

				//para que se active la animacion cuando se hace un match naturalmente al caer//
				foreach (GamePiece piece in matches)
				{


					if (matches.Count == 3)
					{
						int cantidadPuntos = 10 * myCount;

						m_puntaje.SumatoriaPuntos(cantidadPuntos);

					}
					if (matches.Count == 4)
					{
						int cantidadPuntos = 20 * myCount;

						m_puntaje.SumatoriaPuntos(cantidadPuntos);

					}
					if (matches.Count == 5)
					{
						int cantidadPuntos = 30 * myCount;

						m_puntaje.SumatoriaPuntos(cantidadPuntos);

					}
					if (matches.Count >= 6)
					{
						int cantidadPuntos = 40 * myCount;

						m_puntaje.SumatoriaPuntos(cantidadPuntos);

					}

					piece.GetComponentInChildren<Animator>().SetBool("Suicidio", true);
				}

				yield return StartCoroutine(ClearAndCollapseRoutine(matches));
			}
		}
		yield return null;
	}

	//es un rutina que utilizaremos para rellenar la matriz
	IEnumerator RefillRoutine()
	{
		FillBoard(10, .5f);
		yield return null;
	}

	//confirma si ya se realizo el colapso en las columnas
	public bool IsCollapsed(List<GamePiece> gamePieces)
	{
		foreach (GamePiece piece in gamePieces)
		{
			if (piece != null)
			{
				if (piece.transform.position.y - (float)piece.yIndex > 0.001f)
				{
					return false;
				}
			}
		}

		return true;
	}
}


