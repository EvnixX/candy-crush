using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Contador : MonoBehaviour
{
    //variables de enteros para los minutos y segundos
    public int min, seg;

    //variable text mesh pro para visualizar el tiempo en la pantalla de juego 
    public TMP_Text tiempo;

    //variable para visualizar el tiempo que le queda al jugador
    public float restante;

    //avisa si el contador esta en marcha o no 
    public bool enMarcha;

    //calcula el tiempo restante que tiene el jugador con una formula de segundos y minutos
    private void Awake()
    {
        restante = (min * 60) + seg;
    }

    private void Update()
    {
        //una condicion que funciona cuando el contador arranque o no 
        if(enMarcha)
        {
            //se restan los segundos con el tiempo original 
            restante -= Time.deltaTime;
            
            if(restante < 1)
            {
                //cuando el tiempo llegue a 00:00, llevara al jugador a la escena de game over
                SceneManager.LoadScene("Game over");
            }

            // se convierte a entero el temporizador de minutos y segundos basandose en el tiempo restante
            int tempMin = Mathf.FloorToInt(restante / 60);
            int tempSeg = Mathf.FloorToInt(restante % 60);

            //se manda el tiempo en un string para poder mostrarlo en pantalla
            tiempo.text = string.Format("{00:00} : {01:00}", tempMin, tempSeg);
        }
    }

}
