using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePiece : MonoBehaviour
{
    //variable x y
    public int xIndex;
    public int yIndex;

    //se llama al scrpit board para poder utilizarlo
    Board m_board;

    //variable que nos determina las fichas estan en movimientos o no
    bool m_isMoving = false;

    //variables que determinaran el tipo de movimiento que tendra la ficha y de que tipo sera la ficha
    public TipoInterpolacion tipoDeInterpolo;
    public TipoFicha tipoFicha;


    internal void SetCoord(int x, int y)
    {
        xIndex = x;
        yIndex = y;
    }

    //Aqui se inicializa el board
    internal void Init(Board board)
    {
        m_board = board;
    }

    //Aqui se nos permite el movimeinto de las fichas
    internal void Move(int x, int y, float moveTime)
    {
        if (!m_isMoving)
        {
            StartCoroutine(MoveRoutine(x, y, moveTime));
        }
    }

    //Aqui se crea una rutina que nos permitira cambiar las coordenadas y asi poder mover las fichas en la matriz
    //Tambien se determina si es x su tipo de interpolacion para hacer un formula para cambiar su movimiento y velocidad
    IEnumerator MoveRoutine(int destX, int destY, float timeToMove)
    {
        Vector2 startPosition = transform.position;
        bool reacedDestination = false;
        float elapsedTime = 0f;
        m_isMoving = true;

        while (!reacedDestination)
        {
            if (Vector2.Distance(transform.position, new Vector2(destX, destY)) < 0.01f)
            {
                reacedDestination = true;
                if (m_board != null)
                {
                    m_board.placeGamePiece(this, destX, destY);
                }
                break;
            }

            elapsedTime += Time.deltaTime;
            float t = Mathf.Clamp(elapsedTime / timeToMove, 0f, 1f);

            switch (tipoDeInterpolo)
            {
                case TipoInterpolacion.Lineal:

                    break;

                case TipoInterpolacion.Entrada:

                    t = 1 - Mathf.Cos(t * Mathf.PI * .5f);

                    break;

                case TipoInterpolacion.Salida:

                    t = Mathf.Sin(t + Mathf.PI * .5f);

                    break;

                case TipoInterpolacion.Suavizado:

                    t = t * t * (3 - 2 * t);

                    break;

                case TipoInterpolacion.MasSuavizado:

                    t = t * t * t * (t * (t * 6 - 15) + 10);

                    break;
            }

            transform.position = Vector2.Lerp(startPosition, new Vector2(destX, destY), t);

            yield return null;
        }

        m_isMoving = false;
    }

    //Aqui se encunetran los tipos de movimientos para las fichas
    public enum TipoInterpolacion
    {
        Lineal,
        Entrada,
        Salida,
        Suavizado,
        MasSuavizado,
    }

    //Aqui se encuntra de que tipo va a ser cada ficha
    public enum TipoFicha
    {
        Cobre,
        Hierro,
        Oro,
        Esmeralda,
        Diamante,
        Netherite,
       

    }









}
