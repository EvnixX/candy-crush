using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LimitMovements : MonoBehaviour
{
    //llamamos el scrip de puntaje para asi utilizarlo
    public Puntaje movimientos;

    //llamamos el text mesh pro que utilizaremos para demostrar los movimientos que tendra el jugador
    private TextMeshProUGUI TextMesh;

    //llamamos el game manager y asi poder utilizar el tiempo alojado en el script.
    public Contador tiempo;


    //en el start llamamos al componente y lo entregamos a este script para poder utilizarlo
    public void Start()
    {
        TextMesh = GetComponent<TextMeshProUGUI>();
    }


    private void Update()
    {
        //se convierte en formato de texto los movimeintos que se le daran al jugador para ganar 
        TextMesh.text = movimientos.movimientosNecesarios.ToString("0");

        //Se crea una condicional que cuando sus movimiento lleguen a 0 se invoque una setcontion para un game over
        if (movimientos.movimientosNecesarios < 1 || tiempo.restante < 1)
        {
            movimientos.SetCondition();
        }
    }
}
