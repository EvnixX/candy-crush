using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameM : MonoBehaviour
{
    //funciones para el cambio de escena en los botones del proyecto 

    public void PantallaPrincipal()
    {
        SceneManager.LoadScene("Menu");
    }

    public void Juego()
    {
        SceneManager.LoadScene("Nivel1");
    }

    public void Juego2()
    {
        SceneManager.LoadScene("Nivel2");
    }

    public void Juego3()
    {
        SceneManager.LoadScene("Nivel3");
    }

    public void Juego4()
    {
        SceneManager.LoadScene("Nivel4");
    }

    public void Juego5()
    {
        SceneManager.LoadScene("Nivel5");
    }

    public void PantallaNiveles()
    {
        SceneManager.LoadScene("MenuNiveles");
    }

    public void Perdedor()
    {
        SceneManager.LoadScene("Game over");
    }

    public void Ganador()
    {
        SceneManager.LoadScene("Ganaste");
    }

    public void Salir()
    {
        Application.Quit();
    }
}
